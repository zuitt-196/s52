import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';//-> import from app file
// import bootstrap 
import 'bootstrap/dist/css/bootstrap.min.css';
// kung compare sa express parang index>router>controllers

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);






      // JSX 
// const name = "Peter Parker"
// const element = <h1> Hello , {name}</h1>

// const user = {
//   firstName: "Vhong",
//   lastName: "Bercasio"

// }; 

// const formatName = (user) =>{
//     return user.firstName + ' ' + user.lastName;

// }
// const fullName = <h1>Hello, {formatName(user)}</h1>

// const root = ReactDOM.createRoot(document.getElementById('root'));
//   root.render(<fullName></fullName>);