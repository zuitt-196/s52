import {Row, Col, Button} from 'react-bootstrap'

import {Link} from 'react-router-dom';

export default function Banner({data}) {

    const {Title, content, destination, label} = data;

    return (
        <Row>
                <Col claasName = "p-5">
                <h1>{Title}</h1>
                <p>{content}</p>
                     <Button variant="primary" as = {Link} to ={destination}>{label}</Button>
             </Col>
        </Row>

    )


}