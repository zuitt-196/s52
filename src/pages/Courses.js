// import data 
import coursesData from '../data/coursesData'
import CourseCard from '../components/CourseCard' 

export default function Coures() {
    // console.log(coursesData);
    // console.log(coursesData[0])


    // to manipulate the array used the map iterate from the mock up data or to take it inside the components of coursCards
    // as anoymous function make parameter which is course (singular)
    const courses= coursesData.map(course =>{
            return (
                // by used the key as the unique every loop 
                <CourseCard key={course.id} courseProp ={course}/>
            )

    })
    return (
        <>
             <h1>Available</h1>
             {/* props is like function parameter */}
             
             {/* <CourseCard coursesProp = {coursesData[0]}/> */} 
             {courses}
        
        </>
       

    )


}