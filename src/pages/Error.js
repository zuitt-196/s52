
import Banner from '../components/Banner';
export default function Error() {
        const data = {
            Title: "404-not-found",
            content: "The page you are are looking for cannot be found",
            destination:"/",
            label:"Back Home"

        }

    return(

             <Banner data={data}/>
    
    )


}